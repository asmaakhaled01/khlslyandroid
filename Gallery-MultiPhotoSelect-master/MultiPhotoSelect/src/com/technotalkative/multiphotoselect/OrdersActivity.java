package com.technotalkative.multiphotoselect;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;



public class OrdersActivity extends Activity {
	
	ArrayList<String> order_service = new ArrayList<String>();
	ArrayList<String> order_date = new ArrayList<String>();
	ArrayList<String> order_id = new ArrayList<String>();
	ArrayList<String> order_statues = new ArrayList<String>();
	ListView ordersListview;
	//10.0.2.2  10.145.87.72
	//String address = "http://10.0.2.2/testkhlaly/getOrders.php";
	String address = "http://khlsly-alexblog.rhcloud.com/getUserOrders";
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_orders);

		getUserOrders();
		
	    ordersListview = (ListView) findViewById(R.id.OrdersCustomlistView);
		ordersListview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int position,
					long arg3) {
			
				String orderID = order_id.get(position);
            	Intent i = new Intent(OrdersActivity.this, OrderActivity.class);   
            	i.putExtra("orderID", orderID);
            	startActivity(i);
							
			}
			
		});
		 
	}
	
private void getUserOrders() {

    new Thread(new Runnable() { 
        public void run(){
        	
        
	try {
		
	    SharedPreferences logedUser = getSharedPreferences("logedUser", MODE_PRIVATE); 
	    String userID = logedUser.getString("id", null);
	    
	    //List<NameValuePair> pairs = new ArrayList<NameValuePair>();
		//pairs.add(new BasicNameValuePair("id", userID));
	    //Log.i("Seeeee",userID);
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(address+"/"+userID);
		//post.setEntity(new UrlEncodedFormEntity(pairs));
		HttpResponse response = client.execute(httpGet);

		String result = EntityUtils.toString(response.getEntity());
		Log.i("Seeeee result",result);
		JSONObject j = new JSONObject(result);
		
        JSONArray ja = new JSONArray(j.getString("result"));
        Log.i("Array ",ja.toString());
        int n = ja.length();
         
        if(n >= 1){
        for (int i = 0; i < n; i++) {
           
            JSONObject jo = ja.getJSONObject(i);
            Log.i("object ",jo.toString());
            String id = jo.getString("id");
            String date = jo.getString("date").split("\\s+")[0];
            String service_name = jo.getString("service_name");
            String state  = jo.getString("status");		

    		order_service.add(service_name);
    		order_date.add(date);
    		order_id.add(id);
    		order_statues.add(state);	

        }
        
        
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	CustomAdapter customAdapter = new CustomAdapter();
            	ordersListview.setAdapter(customAdapter);
            }
        });
        
        }else{
        	
        	 runOnUiThread(new Runnable() {
                 @Override
                 public void run() {
                 	
                 	ordersListview.setVisibility(View.GONE);
                 	TextView NoOrdersTextView = (TextView) findViewById(R.id.NoOrdersTextView);
                 	NoOrdersTextView.setVisibility(View.VISIBLE);
                 }
             });
        	 
        }
        
		
		
		

	} catch (UnsupportedEncodingException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (ClientProtocolException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	} catch (JSONException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
        }}).start();
		
	}


public class CustomAdapter extends BaseAdapter{

	@Override
	public int getCount() {

		return order_id.size();
	}

	@Override
	public Object getItem(int index) {
		
		return order_id.get(index);
	}

	@Override
	public long getItemId(int arg0) {
		return arg0;
	}

	@Override
	public View getView(int arg0, View arg1, ViewGroup arg2) {
	
        LayoutInflater inflater = (LayoutInflater) OrdersActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        arg1 = inflater.inflate(R.layout.listitem, arg2, false);
        		
   
        TextView ServiceName = (TextView)arg1.findViewById(R.id.customListviewHeader);
        TextView OrderDate = (TextView)arg1.findViewById(R.id.customListviewDesc11);
        ImageView statuesImg = (ImageView)arg1.findViewById(R.id.customListviewImg);
        
        String  service  = order_service.get(arg0);
        String  date  = order_date.get(arg0);
        String  statues  = order_statues.get(arg0);

        if (statues.equals("1") || statues.equals("7") ){
        	statuesImg.setImageDrawable(OrdersActivity.this.getResources().getDrawable(R.drawable.waiting));
        }else if (statues.equals("2")  ){
        	statuesImg.setImageDrawable(OrdersActivity.this.getResources().getDrawable(R.drawable.done));
        	
        }else if (statues.equals("3")){
        	statuesImg.setImageDrawable(OrdersActivity.this.getResources().getDrawable(R.drawable.rejected));
        	
        }else if (statues.equals("9") || statues.equals("10")){
        	statuesImg.setImageDrawable(OrdersActivity.this.getResources().getDrawable(R.drawable.on_deliverey));
        	
        }else{
        	statuesImg.setImageDrawable(OrdersActivity.this.getResources().getDrawable(R.drawable.locked));
        	
        }
       
        ServiceName.setText(service);
        OrderDate.setText(date);
       
        return arg1;
        
	}
	
	
	
	}

}


