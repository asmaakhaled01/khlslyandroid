package com.technotalkative.multiphotoselect;

import android.app.TabActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.Toast;
import android.widget.TabHost.TabSpec;

public class KhlslyActivity extends TabActivity  {

	public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
	    SharedPreferences logedUser = getSharedPreferences("logedUser", MODE_PRIVATE); 
	    
	    /*
	     Editor editor = logedUser.edit();
	     editor.putString("id", null);
	     editor.commit();
	    */
	    String userID = logedUser.getString("id", null);
	    

	    
	    //Toast.makeText(this, userID, Toast.LENGTH_LONG).show();
		
		if(userID == null){
			intentLoginActivity();
		}
        setContentView(R.layout.activity_khlsly);
        
        TabHost tabHost = getTabHost();
         
       
        TabSpec profileTab = tabHost.newTabSpec("Profile");
        profileTab.setIndicator("Profile", getResources().getDrawable(R.drawable.profile));
        Intent profileIntent = new Intent(this, ProfileActivity.class);
        profileTab.setContent(profileIntent);
        
         
        TabSpec servicesTab = tabHost.newTabSpec("Services");        
        servicesTab.setIndicator("Services", getResources().getDrawable(R.drawable.services));
        Intent servicesIntent = new Intent(this, ServicesActivity.class);
        servicesTab.setContent(servicesIntent);
         
        
        TabSpec ordersTab = tabHost.newTabSpec("Orders");
        ordersTab.setIndicator("Orders", getResources().getDrawable(R.drawable.orders));
        Intent ordersIntent = new Intent(this, OrdersActivity.class);
        ordersTab.setContent(ordersIntent);
         
       
        tabHost.addTab(ordersTab); 
        tabHost.addTab(servicesTab); 
        tabHost.addTab(profileTab); 
    }
	
	

	 @Override  
   public boolean onCreateOptionsMenu(Menu menu) {  
       // Inflate the menu; this adds items to the action bar if it is present.  
       getMenuInflater().inflate(R.menu.loged_menu, menu);//Menu Resource, Menu  
       return true;  
   }
	    
   @Override  
   public boolean onOptionsItemSelected(MenuItem item) {  
       if (item.getItemId() == R.id.logoutMenuItem) {  
      	
   	    SharedPreferences logedUser = getSharedPreferences("logedUser", MODE_PRIVATE); 
   	    Editor editor = logedUser.edit();
   	    editor.putString("id", null);
   	    editor.commit();
   	    
   		Intent i = new Intent(this, LoginActivity.class);                      
   		startActivity(i);
   	    
   	     
       }
       else if (item.getItemId() == R.id.homeMenuItem) {  
           
      		Intent i = new Intent(this, KhlslyActivity.class);                      
       		startActivity(i);
           
     }      

       return true;
   } 

	
	
	public void intentLoginActivity (){
		
		Intent i = new Intent(this, LoginActivity.class);                      
		startActivity(i);
	}

}
