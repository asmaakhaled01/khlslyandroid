package com.technotalkative.multiphotoselect;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;



import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;


public class ServicesActivity extends Activity {


	ArrayList<String> services_name = new ArrayList<String>();
	ArrayList<String> services_id = new ArrayList<String>();
	ArrayList<String> organization_name = new ArrayList<String>();
	
	ArrayList<String> search_services_name = new ArrayList<String>();
	ArrayList<String> search_services_id = new ArrayList<String>();
	ArrayList<String> search_organization_name = new ArrayList<String>();
	
	ListView servicesListview;
	AutoCompleteTextView autoCompleteText;
	String address = "http://khlsly-alexblog.rhcloud.com/getServices";
			//"http://khlsly-engyblog.rhcloud.com/getServices";
			//"http://10.0.2.2/testkhlaly/getServices.php"; 
			//"http://10.145.87.72/testkhlaly/getServices.php"
	CustomAdapter customAdapter;
	ArrayAdapter<String> autoCompleteAdapter ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_services);
		
		servicesListview = (ListView) findViewById(R.id.servicesListView);
		autoCompleteText = (AutoCompleteTextView) findViewById(R.id.servicesSearchTextView);
		initListView();
		

	            
	}
	
	
	public void initListView(){

	    new Thread(new Runnable() { 
	        public void run(){
	        		        
		try {
			

			HttpClient client = new DefaultHttpClient();
			HttpGet get = new HttpGet(address);
			HttpResponse response = client.execute(get);
			String result = EntityUtils.toString(response.getEntity());
			
			
			JSONObject j = new JSONObject(result);
			
			
	        JSONArray ja = new JSONArray(j.getString("services"));
	        
	        
	        
	        //ser_name":"ser1","org_name
	        
	        int n = ja.length();

	        for (int i = 0; i < n; i++) {
	            
	            JSONObject jo = ja.getJSONObject(i);

	            String id = jo.getString("id");
	            String name = jo.getString("ser_name");
	            String organization = jo.getString("org_name");
	            
	        	services_name.add(name);
	        	services_id.add(id);
	        	organization_name.add(organization);
	        	
	        	search_services_name.add(name); 
	        	search_services_id.add(id);
	        	search_organization_name.add(organization);

	        }
	        
	        
	        refreshListView();
	        
			
			
			

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	        }}).start();


		
	}
	
	
	private void refreshListView() {
		
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	customAdapter = new CustomAdapter();
            	servicesListview.setAdapter(customAdapter);
            	
            	servicesListview.setOnItemClickListener(new OnItemClickListener() {

					@Override
					public void onItemClick(AdapterView<?> arg0, View arg1,
							int position, long arg3) {
						
						String serviceID = search_services_id.get(position);
		            	
		            	Intent i = new Intent(ServicesActivity.this, ServiceActivity.class);   
		            	i.putExtra("serviceID", serviceID);
		            	startActivity(i);
						
					}
				});
            	//R.layout.custom_drop_down_list_view
    			autoCompleteAdapter = new ArrayAdapter<String>(ServicesActivity.this, R.layout.custom_drop_down_list_view , services_name);
    			autoCompleteText.setThreshold(1);
    			autoCompleteText.setAdapter(autoCompleteAdapter);
            }
        });
		
	}


	public void search (View v){
		
		String searchStr = autoCompleteText.getText().toString().toLowerCase();
		
		String s ; 
		search_services_name = new ArrayList<String>();
		search_services_id = new ArrayList<String>();
		search_organization_name = new ArrayList<String>();
		
		
		for (int i = 0 ; i < services_id.size() ; i ++){
			s = services_name.get(i).toLowerCase();
			
			if (s.indexOf(searchStr) != -1) {
				
				search_services_name.add(services_name.get(i));
				search_services_id.add(services_id.get(i));
				search_organization_name.add(organization_name.get(i));
				
		     }
		}
		
		refreshListView();
		
		
		
	}
	
	
	


	
	
	public class CustomAdapter extends BaseAdapter{

		@Override
		public int getCount() {

			return search_services_id.size();
		}

		@Override
		public Object getItem(int index) {
			
			return search_services_id.get(index);
		}

		@Override
		public long getItemId(int arg0) {
			return arg0;
		}

		@Override
		public View getView(int arg0, View arg1, ViewGroup arg2) {
		
	        LayoutInflater inflater = (LayoutInflater) ServicesActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	        arg1 = inflater.inflate(R.layout.listitem, arg2, false);
	        		
	   
	        TextView ServiceName = (TextView)arg1.findViewById(R.id.customListviewHeader);
	        TextView OrganizationName = (TextView)arg1.findViewById(R.id.customListviewDesc11);
	        ImageView ServiceImg = (ImageView)arg1.findViewById(R.id.customListviewImg);
	        
	        String  service  = search_services_name.get(arg0);
	        String  organization  = search_organization_name.get(arg0);
	        
	        ServiceImg.setImageDrawable(ServicesActivity.this.getResources().getDrawable(R.drawable.done));
	        
	        ServiceName.setText(service);
	        OrganizationName.setText(organization);
	       
	        return arg1;
	        
		}
		
		
		
		}

}

