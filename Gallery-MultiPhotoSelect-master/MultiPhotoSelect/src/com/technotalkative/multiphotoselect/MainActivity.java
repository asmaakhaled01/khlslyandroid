package com.technotalkative.multiphotoselect;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Gallery;
import android.widget.ImageView;
import android.widget.Toast;


public class MainActivity extends Activity {
int intentRequestCode = 1000;
ArrayList<String> selectedItems = new ArrayList<String>();
ArrayList<String> encodedItems;
byte[] byte_arr;
Bitmap bitmap;
String address = "http://10.0.2.2/testkhlaly/uploadImgs.php";
String serviceID ; 

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		Bundle bundle = getIntent().getExtras();
		serviceID = bundle.getString("serviceID");
		
	}
	
 

	public void SendImages() {

        new Thread(new Runnable() { 
            public void run(){
            	
            	BitmapFactory.Options options = null;
                options = new BitmapFactory.Options();
            	options.inSampleSize = 3;
            	List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            	encodedItems = new ArrayList<String>(selectedItems.size());
	
        	    SharedPreferences logedUser = getSharedPreferences("logedUser", MODE_PRIVATE); 
        	    String userID = logedUser.getString("id", null);
        	    
            	pairs.add(new BasicNameValuePair("serviceID", serviceID));
            	pairs.add(new BasicNameValuePair("userID", userID));
            	
            	for (int i =0 ; i < selectedItems.size() ; i ++){
            		
	            	bitmap = BitmapFactory.decodeFile(selectedItems.get(i),options);
	            	ByteArrayOutputStream stream = new ByteArrayOutputStream();
	            	bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream); 
	            	byte_arr = stream.toByteArray();
	            	encodedItems.add(i, Base64.encodeToString(byte_arr, 0));
	            	String filename = "img"+i+".jpg";
					pairs.add(new BasicNameValuePair(filename, encodedItems.get(i)));
				
            	}
				
					try {
					
	            	HttpClient client = new DefaultHttpClient();
	            	HttpPost post = new HttpPost(address);	
					post.setEntity(new UrlEncodedFormEntity(pairs));
					HttpResponse httpResponse = client.execute(post);
					
					InputStream inputStream = httpResponse.getEntity().getContent();
					InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
					BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
					StringBuilder stringBuilder = new StringBuilder();
					String bufferedStrChunk = null;
					while((bufferedStrChunk = bufferedReader.readLine()) != null){
						stringBuilder.append(bufferedStrChunk);
					}
	
					if( stringBuilder.toString().equals("done")){
				    	
						runOnUiThread(new Runnable() {
					        public void run()
					        {
						    	Intent i = new Intent(MainActivity.this, KhlslyActivity.class);   
						    	startActivity(i);
					        }
					    });
						
					}else{
						
						runOnUiThread(new Runnable() {
					        public void run()
					        {
					        	Toast.makeText(getApplicationContext(),"an error occures ", Toast.LENGTH_LONG).show();
					        }
					    });
						
					}
		
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
								
            }
        }).start();

	}
		
	public void load (View v){
		
		Intent i = new Intent(this, MultiPhotoSelectActivity.class);                      
		startActivityForResult(i, intentRequestCode);
	}
	
	public void upload(View v ){

		if(selectedItems.size() >= 1){
			
			SendImages();
			
			
		}else{
			
			Toast.makeText(getApplicationContext(),"You must select image from gallery before you try to upload", Toast.LENGTH_LONG).show();
		}
		
	}
	
	public class ImageAdapter extends BaseAdapter {
		
		private Context context;

		public ImageAdapter(Context c){
			context = c;
		}
		// returns the number of images
		public int getCount() {
			
			return selectedItems.size();
		}

		public Object getItem(int position) {
			
			return position;
		}
	
		public long getItemId(int position) {
			
			return position;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ImageView imageView = new ImageView(context);
			imageView.setImageBitmap(BitmapFactory.decodeFile(selectedItems.get(position)));
			//imageView.setLayoutParams(new Gallery.LayoutParams(100, 100));
			//imageView.setBackgroundResource(itemBackground);
			return imageView;
		}
		
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		selectedItems = new ArrayList<String>();
		
	    if (requestCode == intentRequestCode) {
	        if(resultCode == RESULT_OK){
	        	selectedItems =data.getStringArrayListExtra("selectedItems");
	        	
	        	Gallery gallery = (Gallery) findViewById(R.id.galleryView);
	        	gallery.setAdapter(new ImageAdapter(this));

	        }
	        if (resultCode == RESULT_CANCELED) {
	        	Toast.makeText(getApplicationContext(),"You must select image from gallery before you try to upload", Toast.LENGTH_LONG).show();
	    		
	        }
	    }
	}//onActivityResult
	
}
