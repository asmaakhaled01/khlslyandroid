package com.technotalkative.multiphotoselect;


import java.util.Calendar;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;

public class DateActivity extends Activity {

	TextView v ;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_date);
		
		v = (TextView) findViewById(R.id.textViewTest);
	}
	
	public void showDialog (View v ){
		
		Calendar c = Calendar.getInstance();
	    int mYear = 2016;//c.get(Calendar.YEAR);
	    int mMonth = 8;//c.get(Calendar.MONTH);
	    int mDay = 30;//c.get(Calendar.DAY_OF_MONTH);
	    
	    DatePickerDialog dialog = new DatePickerDialog(DateActivity.this,new mDateSetListener(), mYear, mMonth, mDay);
	    dialog.show();
	}
	
	class mDateSetListener implements DatePickerDialog.OnDateSetListener {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			int mYear = year;
            int mMonth = monthOfYear;
            int mDay = dayOfMonth;
            v.setText(new StringBuilder()
                    // Month is 0 based so add 1
                    .append(mMonth + 1).append("/").append(mDay).append("/")
                    .append(mYear).append(" "));
            System.out.println(v.getText().toString());
			
		}

		
    }
	
}
