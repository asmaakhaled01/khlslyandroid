package com.technotalkative.multiphotoselect;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class LoginActivity extends Activity {

	String givenUsername ;
	String givenPassword ;

	EditText username;
	EditText password;
	
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
	    
	    /*
	     SharedPreferences logedUser = getSharedPreferences("logedUser", MODE_PRIVATE); 
	     Editor editor = logedUser.edit();
	     editor.putString("id", null);
	     editor.commit();
	     */

		/*
	    SharedPreferences logedUser = getSharedPreferences("logedUser", MODE_PRIVATE); 
	    String userID = logedUser.getString("id", null);
		
		if(userID != null){
			Log.i("Current userID ",userID);
			intentHomeActivity();
		*/	
		//}else{
			
		
			username = (EditText) findViewById(R.id.tvUsername);
	    	password = (EditText) findViewById(R.id.tvPassword);
	    	
	    	
		//}
		
	}
	
	 @Override  
	    public boolean onCreateOptionsMenu(Menu menu) {  
	        // Inflate the menu; this adds items to the action bar if it is present.  
	        getMenuInflater().inflate(R.menu.notloged_menu, menu);//Menu Resource, Menu  
	        return true;  
	    }
		    
	    @Override  
	    public boolean onOptionsItemSelected(MenuItem item) {  
	        if (item.getItemId() == R.id.loginMenuItem) {  
	        	
	    		Intent i = new Intent(this, LoginActivity.class);                      
	    		startActivity(i);
	    
	        }
	        else if (item.getItemId() == R.id.registerMenuItem) {  
	          
	  		Intent i = new Intent(this, RegistrationActivity.class);                      
	  		startActivity(i); 
	    }        

	        return true;
	    } 

	    
	
	public void showError (){
		
		runOnUiThread(new Runnable() {
			public void run() {
			    
				LoginActivity.this.password.setText("");
			    Toast.makeText(LoginActivity.this, "invalid username or password", Toast.LENGTH_LONG).show();
			    }
			});	
	}
	
	public void intentHomeActivity (){
		
		Intent i = new Intent(this, KhlslyActivity.class);                      
		startActivity(i);
	
	}
	
	public void intentRegisterActivity(View v ){
		
		Intent i = new Intent(this, RegistrationActivity.class);                      
		startActivity(i);
	}
    
	public void login (View v){
	
    	givenUsername = username.getText().toString();
    	givenPassword = password.getText().toString();
    	
		connectWithHttpGet();
    }

    
	private void connectWithHttpGet() {

        new Thread(new Runnable() { 
            public void run(){        

			    HttpClient httpClient = new DefaultHttpClient();
			  //   10.145.87.72
			    //HttpGet httpGet = new HttpGet("http://10.0.2.2/testkhlaly/getlogin.php?paramUsername=" + givenUsername + "&paramPassword=" + givenPassword );
			    HttpGet httpGet = new HttpGet("http://khlsly-alexblog.rhcloud.com/requesterLogin/" + givenUsername + "/" + givenPassword );
			    
		
					try {
						
						HttpResponse httpResponse = httpClient.execute(httpGet);				
						InputStream inputStream = httpResponse.getEntity().getContent();
						InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
						BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
						StringBuilder stringBuilder = new StringBuilder();
						String bufferedStrChunk = null;
						
						while((bufferedStrChunk = bufferedReader.readLine()) != null){
							stringBuilder.append(bufferedStrChunk);
						}
						
						if( stringBuilder.toString().equals("-1") ){
				       	
 							 showError();						
	
							
						}else{

							
							try {
								   int num = Integer.parseInt(stringBuilder.toString());
							
								     SharedPreferences logedUser = getSharedPreferences("logedUser", MODE_PRIVATE);
								     Editor editor = logedUser.edit();
								     editor.putString("id", stringBuilder.toString());
								     editor.commit();
								     intentHomeActivity();
								     
								} catch (NumberFormatException e) {
									showError();
								}
							
							 
						     
						}
				
					} catch (ClientProtocolException cpe) {
						System.out.println("Exceptionrates caz of httpResponse :" + cpe);
						cpe.printStackTrace();
					} catch (IOException ioe) {
						System.out.println("Secondption generates caz of httpResponse :" + ioe);
						ioe.printStackTrace();
					}
            	
            }
        }).start();

	}
			

	
}
