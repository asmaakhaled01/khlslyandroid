package com.technotalkative.multiphotoselect;


import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class ServiceActivity extends Activity {

	String serviceID ; //10.0.2.2   10.145.87.72
	String address = "http://khlsly-alexblog.rhcloud.com/getService/";
	
			//"http://10.0.2.2/testkhlaly/getService.php";
	
    String id,name,organization,description;
    TextView Organization , Service, SrviceDescription;
    //Button addOrder;
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_service);
		
		Bundle bundle = getIntent().getExtras();
		serviceID = bundle.getString("serviceID");
	   
		Organization = (TextView) findViewById(R.id.ServiceOrganizationtextView);
		Service = (TextView) findViewById(R.id.ServiceNametextView);
		SrviceDescription = (TextView) findViewById(R.id.ServiceDescreptiontextView);
		//addOrder = (Button) (TextView) findViewById(R.id.AddOrderbutton);
		
		getServiceviceData(); 
	}

	public void addOrder(View v){

    	Intent i = new Intent(ServiceActivity.this, MainActivity.class);   
    	i.putExtra("serviceID", serviceID);
    	startActivity(i);
		
	}
	
	private void getServiceviceData(){
	

	    new Thread(new Runnable() { 
	        public void run(){
	        		        
		try {
			
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        	pairs.add(new BasicNameValuePair("serviceID", serviceID));
        	
        	address= address + serviceID;
        	
			HttpClient client = new DefaultHttpClient();
        	HttpGet post = new HttpGet(address);	
			//post.setEntity(new UrlEncodedFormEntity(pairs));
			HttpResponse response = client.execute(post);
			
			String result = EntityUtils.toString(response.getEntity());
	        JSONObject jo = new JSONObject(result);
	        JSONArray ja = new JSONArray(jo.getString("services"));
	        JSONObject jObj = ja.getJSONObject(0);
	        
            id = jObj.getString("id");
            name = jObj.getString("ser_name");
            organization = jObj.getString("org_name");
	        description = jObj.getString("description");
	        
	        viewServiceviceData();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	        }}).start();

		
	}
	
	
	
	private void viewServiceviceData(){
		
		runOnUiThread(new Runnable() {
            @Override
            public void run() {
				Organization.setText(organization);
				Service.setText(name);
				SrviceDescription.setText(description);
            }
        });
		
	}

}
