package com.technotalkative.multiphotoselect;


import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;











import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;



public class OrderActivity extends Activity {

	String statues ; 
	ImageView statuesImg ;
	int intentRequestCode = 1000;
	ArrayList<String> selectedImages = new ArrayList<String>();
	ArrayList<String> encodedImages = new ArrayList<String>();
	//10.145.87.72 10.0.2.2
	
	
	String address = "http://10.0.2.2/testkhlaly/updateOrder.php";
			//"http://khlsly-engyblog.rhcloud.com/updateOrder";
			//"http://10.0.2.2/testkhlaly/updateOrder.php";
	
	String getOrderaddress = "http://10.0.2.2/testkhlaly/getOrder.php";
			//"http://khlsly-engyblog.rhcloud.com/userOrder";
			//"http://10.0.2.2/testkhlaly/getOrder.php";
	
	String orderID ; 
	byte[] byte_arr;
	Bitmap bitmap;	
	TextView Organization, Service , Date , Comment;
	String organization, service , date , comment , state, action_date;
	Gallery imagesGallery ;
	String key;
    JSONObject jo;
    ArrayList<Bitmap> bitmapList = new ArrayList<Bitmap>();
    Button deliveryDate;
    ImageButton uploadImage,submit;
	RadioGroup deliveryType;
	RadioButton deliveryByMyself , deliveryMan; 
    int action_date_Year , action_date_Month , action_date_Day; 

	int delivery=0;
	String deliveryDateStr ="";
	
	StringBuilder stringBuilder;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_order);

		//getActionBar().getDisplayOptions()
		Bundle bundle = getIntent().getExtras();
		orderID = bundle.getString("orderID");
		Organization = (TextView) findViewById(R.id.OrderOrganizationTextView);
		Service = (TextView) findViewById(R.id.OrderServiceTextView);
		Date = (TextView) findViewById(R.id.OrderDateTextView);
		Comment = (TextView) findViewById(R.id.OrderCommentTextView);
		imagesGallery = (Gallery) findViewById(R.id.OrderOldGallery);
		
		uploadImage = (ImageButton)findViewById(R.id.OrderbuttonloadImges);
		deliveryType = (RadioGroup) findViewById(R.id.OrderDeliveryRadioGroup);
		deliveryDate = (Button)findViewById(R.id.OrderDeliveryDateButton);
		submit = (ImageButton)findViewById(R.id.OrderbuttonuploadImges);
		
		deliveryByMyself = (RadioButton)findViewById(R.id.OrderDeliveryRadioByMySelf);
		deliveryMan  = (RadioButton)findViewById(R.id.OrderDeliveryRadioDeliveryMan);
		
		statuesImg = (ImageView) findViewById(R.id.OrderStateImg);
		getOrderData();
		
		
	}

	private void getOrderData(){
		
	    new Thread(new Runnable() { 
	        public void run(){
	        		        
		try {
			
			List<NameValuePair> pairs = new ArrayList<NameValuePair>();
        	pairs.add(new BasicNameValuePair("orderID", orderID));

			HttpClient client = new DefaultHttpClient();
        	//HttpGet post = new HttpGet(getOrderaddress+"/"+orderID);
			HttpPost post = new HttpPost(getOrderaddress);
			post.setEntity(new UrlEncodedFormEntity(pairs));
			HttpResponse response = client.execute(post);
			
			String result = EntityUtils.toString(response.getEntity());
	        jo = new JSONObject(result);
	        Log.i("Response",result);

	        date = jo.getString("date");
            service = jo.getString("service_name");
            organization = jo.getString("organization_name");
	        comment = jo.getString("comment");
	        state = jo.getString("state");
	        action_date = jo.getString("action_date");
	        
	        /*
	        String res = jo.getString("result");
	        JSONArray resJOArr =  new JSONArray(res);        
	        JSONObject resJO = resJOArr.getJSONObject(0);
	         */
	        
	        /*
	        date = resJO.getString("date").split("\\s+")[0];
            service = resJO.getString("service_name");
            organization = resJO.getString("organization_name");
	        comment = resJO.getString("comment");
	        state = resJO.getString("status");
	        action_date = resJO.getString("action_date");
	        */
	        statues= state;
	        
	        
	        
	        runOnUiThread(new Runnable() {
		        public void run()
		        {

			        if (statues.equals("1") || statues.equals("7") ){
			        	statuesImg.setImageDrawable(OrderActivity.this.getResources().getDrawable(R.drawable.waiting));
			        }else if (statues.equals("2")  ){
			        	statuesImg.setImageDrawable(OrderActivity.this.getResources().getDrawable(R.drawable.done));
			        	
			        }else if (statues.equals("3")){
			        	statuesImg.setImageDrawable(OrderActivity.this.getResources().getDrawable(R.drawable.rejected));
			        	
			        }else if (statues.equals("9") || statues.equals("10")){
			        	statuesImg.setImageDrawable(OrderActivity.this.getResources().getDrawable(R.drawable.on_deliverey));
			        	
			        }else{
			        	statuesImg.setImageDrawable(OrderActivity.this.getResources().getDrawable(R.drawable.locked));
			        	
			        }

		        	
		        }
		    });
	        
	        
	        
	        
	        
	        
	        
	        
	    	String[] datetime = action_date.split("\\s+");
	    	String[] date = datetime[0].split("\\-");
	    	
	    	if(state.equals("2")){
	        action_date_Year = Integer.parseInt(date[0]) ;
	        action_date_Month = Integer.parseInt(date[1])-1 ;
	        action_date_Day = Integer.parseInt(date[2]) ;
	    	}
	    	 /*
	    	 res = jo.getString("imgs");
		     resJO =  new JSONObject(res);
	    	
	    	Iterator<?> keys =  resJO.keys();
	        */
	    	Iterator<?> keys =  jo.keys();
	        while( keys.hasNext() ) {
	        
	        	key = (String)keys.next();
	        	
	            if(key.contains("img")){
	            	
	            	String encodedImage = jo.getString(key);
	            	byte[] decodedString = Base64.decode(encodedImage, Base64.DEFAULT);
	            	Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
	            	bitmapList.add(decodedByte);    	
	            }
	        }
	        
	        viewOrderData();

		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	        }}).start();

		
	}
	
	public class GalleryImageAdapter extends BaseAdapter 
	{

		private Context mContext;

	    public GalleryImageAdapter(Context context) 
	    {
	        mContext = context;
	    }
		    
		@Override
		public int getCount() {
			
			return bitmapList.size();
		}

		@Override
		public Object getItem(int arg0) {
			
			return bitmapList.get(arg0);
		}

		@Override
		public long getItemId(int arg0) {
			// TODO Auto-generated method stub
			return arg0;
		}

		@Override
		public View getView(int index, View view, ViewGroup viewGroup) {
			 
	        ImageView i = new ImageView(mContext);
	        i.setImageBitmap(bitmapList.get(index));
	        i.setLayoutParams(new Gallery.LayoutParams(200, 200));	    
	        i.setScaleType(ImageView.ScaleType.FIT_XY);
	        
	        return i;
		}
	    

	}
	

	private void viewOrderData(){
		
		runOnUiThread(new Runnable() {
            @Override
            public void run() {
            	
				Organization.setText(organization);
				Service.setText(service);
				Date.setText(date);
				Comment.setText(comment);
				imagesGallery.setSpacing(1);
				imagesGallery.setAdapter(new GalleryImageAdapter(OrderActivity.this));
				
				//2 or 3 
				if(state.equals("2")){ // accepted need date to deliver & delivery type 
					

					uploadImage.setVisibility(View.GONE);
		            deliveryDate.setText(new StringBuilder()
                    .append(action_date_Month + 1).append("/").append(action_date_Day).append("/")
                    .append(action_date_Year).append(" "));
				
				}else if(state.equals("3")){// rejected need new images 
					
					deliveryType.setVisibility(View.GONE);
					deliveryDate.setVisibility(View.GONE);
					findViewById(R.id.deliveryType).setVisibility(View.GONE); 
					findViewById(R.id.deliveryAddress).setVisibility(View.GONE);
					findViewById(R.id.deliveryAddressTextView).setVisibility(View.GONE);
					findViewById(R.id.deliveryDate).setVisibility(View.GONE);
					
				
					
				}else{ //just view
					
					submit.setVisibility(View.GONE);
					deliveryType.setVisibility(View.GONE);
					deliveryDate.setVisibility(View.GONE);
					uploadImage.setVisibility(View.GONE);
					findViewById(R.id.deliveryType).setVisibility(View.GONE); 
					findViewById(R.id.deliveryAddress).setVisibility(View.GONE);
					findViewById(R.id.deliveryAddressTextView).setVisibility(View.GONE);
					findViewById(R.id.deliveryDate).setVisibility(View.GONE);
					
				}
				
		        
            }
        });
		
	}
	
	
	public void sendData() {
		
		if(state.equals("2")){
			
			Date d = new Date(action_date_Year, action_date_Month, action_date_Day); 
			deliveryDateStr = d.toString();
			
			int deliveryTypeId = deliveryType.getCheckedRadioButtonId();
			if(deliveryTypeId == deliveryMan.getId()) {
				delivery = 1;
			
			} 
			
		}
		
		
		
		
		
        new Thread(new Runnable() { 
            public void run(){
            	

            	List<NameValuePair> pairs = new ArrayList<NameValuePair>();
            	pairs.add(new BasicNameValuePair("orderID", orderID));
            	pairs.add(new BasicNameValuePair("deliveryDate", deliveryDateStr));
            	pairs.add(new BasicNameValuePair("delivery", String.valueOf(delivery)));
            	
            	
            	//address=address+"/"+orderID+"/"+deliveryDateStr+"/"+String.valueOf(delivery);
            	
            	if(selectedImages.size() >= 1 ){
            	encodedImages = new ArrayList<String>(selectedImages.size());
            	
            	BitmapFactory.Options options = null;
                options = new BitmapFactory.Options();
            	options.inSampleSize = 3;
            	
            	for (int i =0 ; i < selectedImages.size() ; i ++){
            		
	            	bitmap = BitmapFactory.decodeFile(selectedImages.get(i),options);
	            	ByteArrayOutputStream stream = new ByteArrayOutputStream();
	            	bitmap.compress(Bitmap.CompressFormat.JPEG, 50, stream); 
	            	byte_arr = stream.toByteArray();
	            	encodedImages.add(i, Base64.encodeToString(byte_arr, 0));
	            	String filename = "img"+i+".jpg";
					pairs.add(new BasicNameValuePair(filename, encodedImages.get(i)));
					//address=address+"/"+encodedImages.get(i);
	            	
				
            	}
            }
					try {
					
	            	HttpClient client = new DefaultHttpClient();
	            	HttpPost post = new HttpPost(address);	
					post.setEntity(new UrlEncodedFormEntity(pairs));
					HttpResponse httpResponse = client.execute(post);
					
					InputStream inputStream = httpResponse.getEntity().getContent();
					InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
					BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
					stringBuilder = new StringBuilder();
					String bufferedStrChunk = null;
					while((bufferedStrChunk = bufferedReader.readLine()) != null){
						stringBuilder.append(bufferedStrChunk);
					}
	
					if( stringBuilder.toString().equals("done")){
				    	
						runOnUiThread(new Runnable() {
					        public void run()
					        {
						    	Intent i = new Intent(OrderActivity.this, KhlslyActivity.class);   
						    	startActivity(i);
					        }
					    });
						
					}else{
						
						runOnUiThread(new Runnable() {
					        public void run()
					        {
					        	Log.i("Responseeee ",stringBuilder.toString());
					        	Toast.makeText(getApplicationContext(),"an error occures "+stringBuilder.toString(), Toast.LENGTH_LONG).show();
					        }
					    });
						
					}
		
				} catch (ClientProtocolException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
								
            }
        }).start();

	}

	
	
	public void uploadData (View v){
		
		
		if(state.equals("2") || (state.equals("3") && selectedImages.size() >= 1 ) ){
			sendData();
		}else{
			
			Toast.makeText(getApplicationContext(),"You must select all the new images before you try to upload", Toast.LENGTH_LONG).show();
		}
	
	}
	
	public void loadImages (View v){
		
		Intent i = new Intent(this, MultiPhotoSelectActivity.class);                      
		startActivityForResult(i, intentRequestCode);
		
	}
	
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		selectedImages = new ArrayList<String>();
		
	    if (requestCode == intentRequestCode) {
	        if(resultCode == RESULT_OK){
	        	selectedImages =data.getStringArrayListExtra("selectedItems");
	        	imagesGallery.setAdapter(new ImageAdapter(this));

	        }
	        if (resultCode == RESULT_CANCELED) {
	        	Toast.makeText(getApplicationContext(),"You must select image from gallery before you try to upload", Toast.LENGTH_LONG).show();
	    		
	        }
	    }
	}//onActivityResult
	
	public class ImageAdapter extends BaseAdapter {
		
		private Context context;

		public ImageAdapter(Context c){
			context = c;
		}
		// returns the number of images
		public int getCount() {
			
			return selectedImages.size();
		}

		public Object getItem(int position) {
			
			return position;
		}
	
		public long getItemId(int position) {
			
			return position;
		}
		
		public View getView(int position, View convertView, ViewGroup parent) {
			
			ImageView imageView = new ImageView(context);
			imageView.setImageBitmap(BitmapFactory.decodeFile(selectedImages.get(position)));
			//imageView.setLayoutParams(new Gallery.LayoutParams(100, 100));
			//imageView.setBackgroundResource(itemBackground);
			return imageView;
		}
		
	}

	

	
	public void getDate (View v){

		    DatePickerDialog dialog = new DatePickerDialog(OrderActivity.this,new mDateSetListener(), action_date_Year, action_date_Month, action_date_Day);
		    dialog.show();
	    
	}
	

	class mDateSetListener implements DatePickerDialog.OnDateSetListener {

		@Override
		public void onDateSet(DatePicker view, int year, int monthOfYear,
				int dayOfMonth) {
			int mYear = year;
            int mMonth = monthOfYear;
            int mDay = dayOfMonth;
            deliveryDate.setText(new StringBuilder()
                    .append(mMonth + 1).append("/").append(mDay).append("/")
                    .append(mYear).append(" "));
           
			
		}

		
    }
	
	
}
