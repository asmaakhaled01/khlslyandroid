package com.technotalkative.multiphotoselect;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ProfileActivity extends Activity {

	EditText etProfileEmail,etProfileUserName;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile);
		

		etProfileEmail = (EditText) findViewById(R.id.etProfileEmail);
		//etProfileEmail.setKeyListener(null);
		etProfileEmail.setEnabled(false);
		etProfileEmail.setText("asmaakhaled01@gmail.com");
		etProfileUserName = (EditText) findViewById(R.id.etProfileUserName);
		//etProfileUserName.setKeyListener(null);
		etProfileUserName.setEnabled(false);
		etProfileUserName.setText("asmaa");
		//Button btnProfileUpdate = (Button) findViewById(R.id.btnProfileUpdate);
	}
	
	public void updateProfile(View v){	
		etProfileEmail.setEnabled(true);
		etProfileUserName.setEnabled(true);
	}
	
	
}
