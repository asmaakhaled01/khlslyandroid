package com.technotalkative.multiphotoselect;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;


public class RegistrationActivity extends Activity {

	String usernameStr ;
	String emailStr ;
	String passwordStr ;

	String ErrorMessage = null;
	
	EditText username;
	EditText email;
	EditText password;
	String address = "http://khlsly-alexblog.rhcloud.com/requesterRegister";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_registration);
		username = (EditText) findViewById(R.id.etUserName);
    	email = (EditText) findViewById(R.id.etEmail);
		password = (EditText) findViewById(R.id.etPass);
 
	}
	
	 @Override  
	    public boolean onCreateOptionsMenu(Menu menu) {  
	        // Inflate the menu; this adds items to the action bar if it is present.  
	        getMenuInflater().inflate(R.menu.notloged_menu, menu);//Menu Resource, Menu  
	        return true;  
	    }
		    
	    @Override  
	    public boolean onOptionsItemSelected(MenuItem item) {  
	        if (item.getItemId() == R.id.loginMenuItem) {  
	        	
	    		Intent i = new Intent(this, LoginActivity.class);                      
	    		startActivity(i);
	    
	        }
	        else if (item.getItemId() == R.id.registerMenuItem) {  
	          
	  		Intent i = new Intent(this, RegistrationActivity.class);                      
	  		startActivity(i); 
	    }        

	        return true;
	    }
	    
	    
		public void register (View v){
			
	    	usernameStr = username.getText().toString();
	    	emailStr = email.getText().toString();
	    	passwordStr = password.getText().toString();
	    	if(usernameStr.length() < 1 || emailStr.length() < 1 || passwordStr.length() <1){
	    		
	    		Toast.makeText(this,"You have to fill username, email and password ", Toast.LENGTH_LONG).show();
	    		
	    	}else if (passwordStr.length() < 6){
	    		
	    		Toast.makeText(this,"Minimum Password length is 6 characters ", Toast.LENGTH_LONG).show();
	    		
	    	}else if (usernameStr.length() < 6){
	    		
	    		Toast.makeText(this,"Minimum Username length is 6 characters ", Toast.LENGTH_LONG).show();
	    		
	    	}else if (!isValidEmail(emailStr)){
	    		
	    		Toast.makeText(this,"Enter a valid email ", Toast.LENGTH_LONG).show();
	    		
	    	}
	    	else{
	    		
			connectWithHttpGet();
			
	    	}
	    }

		public final static boolean isValidEmail(CharSequence target) {
			  return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
			}
		
	    
		private void connectWithHttpGet() {

	        new Thread(new Runnable() { 
	            public void run(){        

						try {
							
							List<NameValuePair> pairs = new ArrayList<NameValuePair>();
							pairs.add(new BasicNameValuePair("username", usernameStr));
							pairs.add(new BasicNameValuePair("email", emailStr));
							pairs.add(new BasicNameValuePair("password", passwordStr));
							
							HttpClient client = new DefaultHttpClient();
							HttpGet httpGet = new HttpGet(address+"/"+usernameStr+"/"+emailStr+"/"+passwordStr);	
							//post.setEntity(new UrlEncodedFormEntity(pairs));
							HttpResponse httpResponse = client.execute(httpGet);
							
							InputStream inputStream = httpResponse.getEntity().getContent();
							InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
							BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
							StringBuilder stringBuilder = new StringBuilder();
							String bufferedStrChunk = null;
							while((bufferedStrChunk = bufferedReader.readLine()) != null){
								stringBuilder.append(bufferedStrChunk);
							}
							
							
							if( stringBuilder.toString().equals("-1") ){
					       	
								ErrorMessage = "Username already exist";
								
							}else if( stringBuilder.toString().equals("-2") ){
								
								ErrorMessage = "email already used";
								
							}else{
							
								Log.i("Resposeee",stringBuilder.toString());
								
								try {
									   int num = Integer.parseInt(stringBuilder.toString());
								 SharedPreferences logedUser = getSharedPreferences("logedUser", MODE_PRIVATE);
							     Editor editor = logedUser.edit();
							     editor.putString("id", stringBuilder.toString());
							     editor.commit();
							     intentHomeActivity();	
								}catch(NumberFormatException e){
									
									ErrorMessage = "Connection Error";
								}
								
							}
							showError ();
						} catch (ClientProtocolException cpe) {
							System.out.println("Exceptionrates caz of httpResponse :" + cpe);
							cpe.printStackTrace();
						} catch (IOException ioe) {
							System.out.println("Secondption generates caz of httpResponse :" + ioe);
							ioe.printStackTrace();
						}
	            	
	            }
	        }).start();

		}
				


		public void showError (){
			
			runOnUiThread(new Runnable() {
				public void run() {
				    if(ErrorMessage != null){
					RegistrationActivity.this.password.setText("");
				    Toast.makeText(RegistrationActivity.this, ErrorMessage, Toast.LENGTH_LONG).show();
				    ErrorMessage = null;
				    }
				    }
				});	
		}
		
		public void intentHomeActivity (){
			
			Intent i = new Intent(this, KhlslyActivity.class);                      
			startActivity(i);
		
		}
		
	}
